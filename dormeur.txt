                 Le dormeur du val 

<<<<<<< HEAD
1 : C'est un trou de verdure où chante une rivière,
=======
C'est un trou de verdure où chante une rivière,
>>>>>>> casse
Accrochant follement aux herbes des haillons
D'argent ; où le soleil, de la montagne fière,
Luit : c'est un petit val qui mousse de rayons.

<<<<<<< HEAD
2 : Un soldat jeune, bouche ouverte, tête nue,
=======
Un soldat jeune, bouche ouverte, tête nue,
>>>>>>> casse
Et la nuque baignant dans le frais cresson bleu,
Dort ; il est étendu dans l'herbe, sous la nue,
Pâle dans son lit vert où la lumière pleut.

<<<<<<< HEAD
3 : Les pieds dans les glaïeuls, il dort. Souriant comme
Sourirait un enfant malade, il fait un somme :
Nature, berce-le chaudement : il a froid.

4 : Les parfums ne font pas frissonner sa narine ;
=======
Les pieds dans les glaïeuls, il dort. Souriant comme
Sourirait un enfant malade, il fait un somme :
Nature, berce-le chaudement : il a froid.

Les parfums ne font pas frissonner sa narine ;
>>>>>>> casse
Il dort dans le soleil, la main sur sa poitrine,
Tranquille. Il a deux trous rouges au côté droit.

           Arthur Rimbaud
